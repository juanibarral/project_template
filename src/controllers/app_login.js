/**
 * app_login.js
 * Controller for login
 * @author: Juan Camilo Ibarra
 * @Creation_Date: March 2016
 * @version: 0.1.0
 * @Update_Author : Juan Camilo Ibarra
 * @Date: March 2016
 */

var my_app = require("./_app_core").my_app;

my_app.controller('login_ctrl', ['$scope', '$http', '$location','dialog_srv', 'auth_srv',function($scope, $http, $location, dialog_srv, auth_srv){
	
	$scope.login = function()
    {
    	if($scope.user)
    	{
    		if(!$scope.user.login)
    			dialog_srv.showDialog('Empty fields', 'please fill user field');
    		else if(!$scope.user.password)
    			dialog_srv.showDialog('Empty fields', 'please fill password field');
    		else
    			$http({
					method : 'POST',
					url : '/api/authenticate',
					headers: {'Content-Type': 'application/x-www-form-urlencoded'},
					data : 'login=' + $scope.user.login + '&password=' + $scope.user.password
				}).then(
					function successCallback(response){
						if(response.data.success)
						{
							auth_srv.setToken(response.data.token);
							//auth_srv.callREST({method : 'get', url : '/#/home'});
							$location.path('/home');
						}
						else
						{
							//$scope.open();
						}
					},
					function errorCallback(response){
						console.log(response);
					}
				);
    	}
    	else
    	{
    		dialog_srv.showDialog('Empty fields', 'please fill user and password fields');
    	}
    };
    
    $scope.keyPressed = function(clickEvent)
    {
    	
    	if(clickEvent.keyCode == 13)
    	{
    		$scope.login();
    	}
    };
}]);
