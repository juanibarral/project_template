
/**
 * app_core_services.js
 * Services for my project 
 * @author: Juan Camilo Ibarra
 * @Creation_Date: March 2016
 * @version: 0.1.0
 * @Update_Author : Juan Camilo Ibarra
 * @Date: May 2016
 */

var my_app = require("./_app_core").my_app;

my_app.factory('auth_srv', [ '$http', function($http){
	var token = null;
	
	var setToken = function(tk)
	{
		token = tk;
	};
	
	var validateUser = function(callback)
	{
		if(token)
		{
			$http({
				method : 'POST',
				url : '/api/validate',
				headers: {'x-access-token': token}
			}).then(
				function successCallback(response){
					callback({type : 'success', response : response});
				},
				function errorCallback(response){
					callback({type : 'error', response : response});
				}
			);
		}
		else
		{
			callback({type : 'error', response : "No token"});
		}
		
	};
	
	return {
		setToken : setToken,
		validateUser : validateUser
	};
}]);

my_app.factory('dialog_srv', [ '$mdDialog', function($mdDialog){
    var showDialog = function(title, message)
    {
    	alert = $mdDialog.alert()
    			.title(title)
    			.content(message)
    			.ok('Close');
    		$mdDialog
    			.show(alert)
    			.finally(function(){
    				alert = unefined;
    			});
    };

    return {
    	showDialog : showDialog,
    }
}]);