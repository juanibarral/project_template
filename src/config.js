/**
 * config.js
 * Configuration for FeniciaViz 
 * @author: Juan Camilo Ibarra
 * @Creation_Date: March 2016
 * @version: 0.1.0
 * @Update_Author : Juan Camilo Ibarra
 * @Date: March 2016
 */

var PORT = 3800;

var CONN = {
	geo_type : '',
	geo_host : '',
	geo_user : '',
	geo_password : '',
	geo_database : '',

	dat_type : '',
	dat_host : '',
	dat_user : '',
	dat_password : '',
	dat_database : '',
}

module.exports = {
	PORT : PORT,
	CONN : CONN
};
