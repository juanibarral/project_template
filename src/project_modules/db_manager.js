/**
 * db_manager.js
 * Module to manage the communication with the database
 * @author: Juan Camilo Ibarra
 * @Creation_Date: March 2016
 * @version: 0.1.0
 * @Update_Author : Juan Camilo Ibarra
 * @Date: March 2016
 */

var db = require('geotabuladb');
var CONFIG = require('../config.js');

var connect = function()
{
	db.setCredentials({
		type : CONFIG.CONN.dat_type,
		host : CONFIG.CONN.dat_host,
		user : CONFIG.CONN.dat_user,
		password : CONFIG.CONN.dat_password,
		database : CONFIG.CONN.dat_database,
	});
}
/**
 * Function to get data from database 
 * @param {Object} params params for the query from client
 * @param {Object} callback function to return retrieved data
 */
var getData = function(params, callback)
{
	//Get something from your database
	callback("some data from database");
};


module.exports = {
	getData : getData
};
