# Backend and Frontend Template #

This is a template for a project using NodeJS, Express, Angular 1.x and Google Material.

### Setup ###

* Fork repository
* Install template
```
> install.sh
```
* Generate development files for the first time
```
> gulp generate_dev
```
* Go to build folder `dev`
```
> cd ../dev
```
* Install project
```
> install.sh
```
* And finally, run project
```
> run.sh
```
* The default user is test and its password is test. This will take you to the main page that has two tabs: One with a map, and the second one with only text.